using System;
using System.Linq;

using Godot;

namespace IsometricTactics;

[GlobalClass]
public partial class CompositeRangeFunc : RangeFunc
{
    [Export] private Operator _operator = Operator.All;
    [Export] private RangeFunc[] _rangeFuncs = Array.Empty<RangeFunc>();

    public CompositeRangeFunc() {}

    public CompositeRangeFunc(Operator @operator, params RangeFunc[] rangeFuncs)
    {
        _operator = @operator;
        _rangeFuncs = rangeFuncs;
    }

    public override bool IsTileInRange(Vector2I fromTile, Vector2I toTile)
    {
        return _operator switch
        {
            Operator.All => _rangeFuncs.All(func => func.IsTileInRange(fromTile, toTile)),
            Operator.Any => _rangeFuncs.Any(func => func.IsTileInRange(fromTile, toTile)),
            Operator.None => !_rangeFuncs.Any(func => func.IsTileInRange(fromTile, toTile)),
            _ => throw new ArgumentOutOfRangeException()
        };
    }

    public enum Operator
    {
        All,
        Any,
        None
    }
}
