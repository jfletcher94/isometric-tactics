using System;

using Godot;

namespace IsometricTactics;

public partial class CommitPanelButton : Button
{
    [Export] public CommitButtonType Type { get; private set; }

    public void OnButtonPressed()
    {
        switch (Type)
        {
            case CommitButtonType.Commit:
                GetParent<ButtonPanel>().LevelTileMap.OnCommitButtonPressed();
                break;
            case CommitButtonType.EndTurn:
                GetParent<ButtonPanel>().LevelTileMap.AdvanceTurn();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public enum CommitButtonType
    {
        Commit,
        EndTurn
    }
}
