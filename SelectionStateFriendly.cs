using System;
using System.Collections.Generic;

using Godot;

namespace IsometricTactics;

public class SelectionStateFriendly : ISelectionState
{
    private readonly SelectionStateNone _selectionStateNone;
    private Vector2I _tile;
    private IPawn _pawn;
    private ActionType _actionType = ActionType.None;
    private Vector2I? _target;

    internal SelectionStateFriendly(SelectionStateNone selectionStateNone)
    {
        _selectionStateNone = selectionStateNone;
    }

    public ISelectionState.Type StateType()
    {
        return ISelectionState.Type.FriendlyType;
    }

    public ISelectionState Select(TileMapHelper tileMap, Vector2I? tile)
    {
        if (_actionType == ActionType.None)
        {
            return ResetHelper().SelectHelper(tileMap, tile);
        }

        if (_target == tile)
        {
            return this;
        }

        if (_target == null)
        {
            if (tile != null && _pawn.CanPerformActionAtTile(_actionType, (Vector2I)tile))
            {
                return SelectHelper(_tile, _pawn, _actionType, tile);
            }

            return this;
        }

        if (tile != null && _pawn.CanPerformActionAtTile(_actionType, (Vector2I)tile))
        {
            return SelectHelper(_tile, _pawn, _actionType, tile);
        }

        return SelectHelper(_tile, _pawn, _actionType);
    }

    public ISelectionState Deselect()
    {
        if (_target != null)
        {
            return SelectHelper(_tile, _pawn, _actionType);
        }

        if (_actionType != ActionType.None)
        {
            return SelectHelper(_tile, _pawn);
        }

        return Reset();
    }

    public ISelectionState Reset()
    {
        return ResetHelper();
    }

    public ISelectionState ChooseAction(ActionType actionType)
    {
        if (_actionType != actionType)
        {
            _target = null;
        }
        if (_pawn.CanPerformAction(actionType))
        {
            _actionType = actionType;
            if (IPawnAction.SelectOwnTileByDefault(_actionType))
            {
                _target = _tile;
            }
        }
        else
        {
            _actionType = ActionType.None;
        }
        return this;
    }

    public Vector2I? GetSelectedTile()
    {
        return _tile;
    }

    public bool CanEndTurn()
    {
        // Protect against accidentally ending turn instead of committing action
        return false;
    }

    public bool CanValidActionBeCommitted(TileMapHelper tileMap)
    {
        return _actionType != ActionType.None
            && _target != null
            && (tileMap.GetPawnAt(_tile)?.CanPerformActionAtTile(_actionType, (Vector2I)_target)
                ?? false);
    }

    public (Node, StringName)? PerformAction(TileMapHelper tileMap)
    {
        return _actionType == ActionType.None || _target == null
            ? null
            : (tileMap.GetPawnAt(_tile)!.AsNode(),
                tileMap.GetPawnAt(_tile)!.PerformActionAtTile(_actionType, (Vector2I)_target));
    }

    public IEnumerable<Vector2I> GetPathToTarget(TileMapHelper tileMap, bool inclusive)
    {
        return _target == null
            ? Array.Empty<Vector2I>()
            : tileMap.GetConnectingTiles(_tile, (Vector2I)_target, inclusive);
    }

    public bool IsTileInActionRange(int x, int y)
    {
        return _pawn.IsTileInActionRange(_actionType, x, y);
    }

    public bool CanPerformActionAtTile(int x, int y)
    {
        return _pawn.CanPerformActionAtTile(_actionType, x, y);
    }

    public bool IsTileTargeted(int x, int y)
    {
        return _actionType != ActionType.None
            && _target != null
            && ((Vector2I)_target).X == x
            && ((Vector2I)_target).Y == y;
    }

    public bool IsTileInEffectArea(int x, int y)
    {
        return _pawn.IsTileInEffectArea(_actionType, _target, x, y);
    }

    public bool IsCursorTileValid(Vector2I? tile)
    {
        // If tile is null, cursor validity is irrelevant; true here is for consistency
        return tile == null
            || _actionType == ActionType.None
            || CanPerformActionAtTile(((Vector2I)tile).X, ((Vector2I)tile).Y);
    }

    public bool IsSelectedTileValid()
    {
        return true;
    }

    internal ISelectionState SelectHelper(
        Vector2I tile, IPawn pawn, ActionType actionType = ActionType.None, Vector2I? target = null)
    {
        _tile = tile;
        _pawn = pawn;
        _actionType = actionType;
        _target = target;
        pawn.SetSelected(true);
        return this;
    }

    private SelectionStateNone ResetHelper()
    {
        _pawn.SetSelected(false);
        return _selectionStateNone;
    }
}
