using System;
using System.Collections.Generic;
using System.Linq;

using Godot;

namespace IsometricTactics;

[GlobalClass]
public partial class TileMapHelper : TileMap
{
    public const float BaseFocusDamageMultiplier = 6f;
    public static readonly StringName Empty = new();

    public static readonly Comparer<Vector2I> Vector2IComparer =
        Comparer<Vector2I>.Create((u, v) => u.X != v.X ? u.X.CompareTo(v.X) : u.Y.CompareTo(v.Y));

    [Signal] public delegate void UpdateSelectedEventHandler();

    public TeamType ActiveTeam { get; private set; } = TeamType.None;

    private const string WriteDataCmdlineArg = "write-data";

    private const int TileExistsBitShift = 0;
    private const int CursorBitShift = 1;
    private const int SelectedBitShift = 2;
    private const int ActionRangeBitShift = 3;
    private const int ActionValidBitShift = 4;
    private const int TargetBitShift = 5;
    private const int ReadyPawnBitShift = 6;
    private const int AreaOfEffectBitShift = 7;
    private const int TilePathBitShift = 8;

    // Move from TileMapHelperNodeTest if needed here
    protected static readonly StringName CellSizeName = new("cell_size");
    protected static readonly StringName SizeMinName = new("size_min");
    protected static readonly StringName SizeMaxName = new("size_max");
    protected static readonly StringName TilesBitsName = new("tiles_bits");
    protected static readonly StringName CursorTileValidName = new("cursor_tile_valid");
    protected static readonly StringName SelectedTileValidName = new("selected_tile_valid");

    private const int MaxBitsColorCacheSize = 256;
    private static readonly Dictionary<uint, Color> BitsColorCache = new(MaxBitsColorCacheSize);
    protected ShaderMaterial ShaderMaterial => (ShaderMaterial)Material;

    protected Vector2I SizeMin;
    protected Vector2I SizeMax;
    protected Image ImageBits;
    protected ImageTexture TilesBits;
    protected bool TileBitsDirty = false;

    private readonly TeamParent?[] _teams = new TeamParent[(int)TeamType.Count];
    protected ISelectionState SelectionState = ISelectionState.InitNew();
    protected bool AcceptingInput = true;
    protected Vector2I? CursorPos = null;
    protected IPawn? LastSelectedPawn = null;

    public override void _EnterTree()
    {
        CalculateGridDimensions();
    }

    protected void CalculateGridDimensions()
    {
        int xMin = Int32.MaxValue, yMin = Int32.MaxValue, xMax = Int32.MinValue,
            yMax = Int32.MinValue;
        foreach (Vector2I cell in GetUsedCells(0))
        {
            if (cell.X < xMin)
            {
                xMin = cell.X;
            }
            if (cell.Y < yMin)
            {
                yMin = cell.Y;
            }
            if (cell.X > xMax)
            {
                xMax = cell.X;
            }
            if (cell.Y > yMax)
            {
                yMax = cell.Y;
            }
        }

        SizeMin = new Vector2I(xMin, yMin);
        SizeMax = new Vector2I(xMax, yMax);

        ImageBits = Image.Create(
            1 + SizeMax.X - SizeMin.X, 1 + SizeMax.Y - SizeMin.Y, false, Image.Format.Rf
        );
        TilesBits = ImageTexture.CreateFromImage(ImageBits);

        ShaderMaterial.SetShaderParameter(CellSizeName, TileSet.TileSize);
        ShaderMaterial.SetShaderParameter(SizeMinName, SizeMin);
        ShaderMaterial.SetShaderParameter(SizeMaxName, SizeMax);
        ShaderMaterial.SetShaderParameter(TilesBitsName, TilesBits);
    }

    public override void _Ready()
    {
        if (!_teams.OfType<TeamParent>().Any())
        {
            throw new ApplicationException("No teams registered");
        }

        AdvanceTurn();
    }

    public override void _Process(double delta)
    {
        if (!TileBitsDirty)
        {
            return;
        }

        // TODO efficiency?
        SortedList<Vector2I, uint> targetedBitPackingInfos = new(Vector2IComparer);
        foreach (var bitPackingInfo in GetTargetedBitPackingInfos())
        {
            foreach (Vector2I tile in bitPackingInfo.Item1)
            {
                targetedBitPackingInfos[tile] = targetedBitPackingInfos.GetValueOrDefault(tile, 0u)
                    | (1u << bitPackingInfo.Item2);
            }
        }
        uint result;
        for (int i = 0; i <= SizeMax.X - SizeMin.X; i++)
        {
            for (int j = 0; j <= SizeMax.Y - SizeMin.Y; j++)
            {
                result = targetedBitPackingInfos.GetValueOrDefault(new Vector2I(i, j), 0u);
                foreach (var bitPackingInfo in GetUntargetedBitPackingInfos())
                {
                    if (bitPackingInfo.Item1.Invoke(i, j))
                    {
                        result |= 1u << bitPackingInfo.Item2;
                    }
                }
                ImageBits.SetPixel(i, j, GetBitsColor(result));
            }
        }

        TilesBits.Update(ImageBits);
        ShaderMaterial.SetShaderParameter(TilesBitsName, TilesBits);
        ShaderMaterial.SetShaderParameter(
            CursorTileValidName, SelectionState.IsCursorTileValid(CursorPos)
        );
        ShaderMaterial.SetShaderParameter(
            SelectedTileValidName, SelectionState.IsSelectedTileValid()
        );
        TileBitsDirty = false;
    }

    public override void _UnhandledInput(InputEvent @event)
    {
        // TODO alternate input
        if (!AcceptingInput)
        {
            return;
        }

        switch (@event)
        {
            case InputEventMouseButton mouseButton:
            {
                if (mouseButton.Pressed)
                {
                    switch (mouseButton.ButtonIndex)
                    {
                        case MouseButton.Left:
                            TileBitsDirty = true;
                            SelectionState = SelectionState.Select(
                                this,
                                TileAtPosition(
                                    ViewportPositionToGlobalPosition(mouseButton.Position)
                                )
                            );
                            EmitSignal(SignalName.UpdateSelected);
                            break;
                        case MouseButton.Right:
                            TileBitsDirty = true;
                            SelectionState = SelectionState.Deselect();
                            EmitSignal(SignalName.UpdateSelected);
                            break;
                    }
                }
                break;
            }
            case InputEventMouseMotion mouseMotion:
            {
                Vector2I? cursorPos =
                    TileAtPosition(ViewportPositionToGlobalPosition(mouseMotion.Position));
                if (!TileExists(cursorPos))
                {
                    cursorPos = null;
                }
                if (CursorPos == cursorPos)
                {
                    return;
                }

                TileBitsDirty = true;
                CursorPos = cursorPos;
                break;
            }
        }
    }

    public IEnumerable<Vector2I> GetAllTiles()
    {
        return GetUsedCells(0).Select(tile => tile - SizeMin);
    }

    /// <summary>
    /// Uses Bresenham's line algorithm.
    /// TODO Pathfinding (e.g. around corners)?
    /// </summary>
    public IEnumerable<Vector2I> GetConnectingTiles(
        Vector2I fromTile, Vector2I toTile, bool inclusive)
    {
        return inclusive
            ? GetConnectingTilesInclusive(fromTile, toTile)
            : GetConnectingTilesExclusive(fromTile, toTile);
    }

    private IEnumerable<Vector2I> GetConnectingTilesExclusive(Vector2I fromTile, Vector2I toTile)
    {
        foreach (Vector2I tile in GetConnectingTilesInclusive(fromTile, toTile))
        {
            // TODO there's probably a better way
            if (tile == fromTile || tile == toTile)
            {
                continue;
            }
            yield return tile;
        }
    }

    private IEnumerable<Vector2I> GetConnectingTilesInclusive(Vector2I fromTile, Vector2I toTile)
    {
        if (fromTile == toTile)
        {
            yield return fromTile;
            yield break;
        }

        int dx = toTile.X - fromTile.X;
        int dy = toTile.Y - fromTile.Y;
        int xInc = Math.Sign(dx);
        int yInc = Math.Sign(dy);
        if (Math.Abs(dy) < Math.Abs(dx))
        {
            int diff = 2 * yInc * dy - xInc * dx;
            int y = fromTile.Y;
            for (int x = fromTile.X; xInc > 0 ? x <= toTile.X : x >= toTile.X; x += xInc)
            {
                yield return new Vector2I(x, y);
                if (diff > 0)
                {
                    y += yInc;
                    diff += 2 * (yInc * dy - xInc * dx);
                }
                else
                {
                    diff += 2 * yInc * dy;
                }
            }
        }
        else
        {
            int diff = 2 * xInc * dx - yInc * dy;
            int x = fromTile.X;
            for (int y = fromTile.Y; yInc > 0 ? y <= toTile.Y : y >= toTile.Y; y += yInc)
            {
                yield return new Vector2I(x, y);
                if (diff > 0)
                {
                    x += xInc;
                    diff += 2 * (xInc * dx - yInc * dy);
                }
                else
                {
                    diff += 2 * xInc * dx;
                }
            }
        }
    }

    public void RegisterTeam(TeamParent teamParent)
    {
        if (GetTeamParent(teamParent.Team) != null)
        {
            throw new ArgumentException(teamParent.Team.ToString());
        }

        _teams[(int)teamParent.Team] = teamParent;
    }

    public TeamParent? GetTeamParent(TeamType team)
    {
        return team is TeamType.None or TeamType.Count ? null : _teams[(int)team];
    }

    public Vector2? TileToGlobalPosition(Vector2I? tile)
    {
        return !TileExists(tile) ? null : TileCoordinatesToLocalPosition((Vector2I)tile!);
    }

    public Vector2I? TileAtPosition(Vector2 position)
    {
        Vector2I tile = LocalPositionToTileCoordinates(GlobalPositionToLocalPosition(position));
        return TileExists(tile) ? tile : null;
    }

    public bool TileExists(Vector2I? tile, int layer = 0)
    {
        return tile != null && GetCellSourceId(layer, (Vector2I)tile + SizeMin) >= 0;
    }

    public bool TileExists(int x, int y)
    {
        return TileExists(new Vector2I(x, y));
    }

    public Vector2? GetFromTileToTile(Vector2I? fromTile, Vector2I? toTile)
    {
        return TileToGlobalPosition(toTile) - TileToGlobalPosition(fromTile);
    }

    public IPawn? GetPawnAt(Vector2I? tile)
    {
        return tile == null
            ? null
            : _teams.OfType<TeamParent>()
                .Select(teamParent => teamParent.GetPawn((Vector2I)tile))
                .FirstOrDefault(pawn => pawn != null && tile.Equals(pawn.Tile));
    }

    public IEnumerable<IPawn> GetAllPawnsForTeam(TeamType team)
    {
        return GetTeamParent(team)?.GetPawns() ?? Array.Empty<IPawn>();
    }

    public IEnumerable<IPawn> GetAllPawns()
    {
        for (TeamType team = 0; team < TeamType.Count; team++)
        {
            foreach (IPawn pawn in GetAllPawnsForTeam(team))
            {
                yield return pawn;
            }
        }
    }

    public bool CanSelectedTilePerformAction(ActionType actionType)
    {
        if (!AcceptingInput)
        {
            return false;
        }

        IPawn? pawn = GetPawnAt(SelectionState.GetSelectedTile());
        return pawn?.Team == ActiveTeam && pawn.CanPerformAction(actionType);
    }

    public bool IsCommitPanelButtonValid(CommitPanelButton.CommitButtonType type)
    {
        return type switch
        {
            CommitPanelButton.CommitButtonType.Commit => CanValidActionBeCommitted(),
            CommitPanelButton.CommitButtonType.EndTurn => SelectionState.CanEndTurn(),
            _ => false
        };
    }

    public void AdvanceTurn()
    {
        TileBitsDirty = true;
        LastSelectedPawn = null;
        SelectionState = SelectionState.Reset();
        GetTeamParent(ActiveTeam)?.AdvanceTurn();
        EmitSignal(SignalName.UpdateSelected);
        while (_teams[(int)(++ActiveTeam == TeamType.Count ? ActiveTeam = 0 : ActiveTeam)]
            == null) {}
    }

    public void OnActionButtonPressed(ActionType actionType)
    {
        if (!AcceptingInput)
        {
            return;
        }

        SelectionState = SelectionState.ChooseAction(actionType);
        EmitSignal(SignalName.UpdateSelected);
        TileBitsDirty = true;
    }

    public void OnCommitButtonPressed()
    {
        if (!CanValidActionBeCommitted())
        {
            return;
        }

        AcceptingInput = false;
        EmitSignal(SignalName.UpdateSelected);
        LastSelectedPawn = GetPawnAt(SelectionState.GetSelectedTile());
        if (SelectionState.PerformAction(this) is (not null, not null) signal
            && !Empty.Equals(signal.Item2))
        {
            signal.Item1.Connect(
                signal.Item2, Callable.From(OnActionsFinished), (uint)ConnectFlags.OneShot
            );
        }
        else
        {
            OnActionsFinished();
        }
    }

    private bool CanValidActionBeCommitted()
    {
        return AcceptingInput && SelectionState.CanValidActionBeCommitted(this);
    }

    private void OnActionsFinished()
    {
        SelectionState = SelectionState.Reset();
        if (LastSelectedPawn?.IsReady() ?? false)
        {
            SelectionState = SelectionState.Select(this, LastSelectedPawn.Tile);
        }
        TileBitsDirty = true;
        AcceptingInput = true;
        EmitSignal(SignalName.UpdateSelected);
    }

    protected Vector2 ViewportPositionToGlobalPosition(Vector2 position)
    {
        return position * GetViewport().CanvasTransform.AffineInverse();
    }

    protected Vector2 GlobalPositionToViewportPosition(Vector2 position)
    {
        return position * GetViewport().CanvasTransform;
    }

    protected Vector2 GlobalPositionToLocalPosition(Vector2 position)
    {
        return position * Transform;
    }

    protected Vector2 LocalPositionToGlobalPosition(Vector2 position)
    {
        return position * Transform.AffineInverse();
    }

    protected Vector2I LocalPositionToTileCoordinates(Vector2 position)
    {
        return new Vector2I(
            -SizeMin.X
            + Mathf.FloorToInt(
                0.5f + position.X / TileSet.TileSize.X - position.Y / TileSet.TileSize.Y
            ),
            -SizeMin.Y
            + Mathf.FloorToInt(
                -0.5f + position.X / TileSet.TileSize.X + position.Y / TileSet.TileSize.Y
            )
        );
    }

    protected Vector2 TileCoordinatesToLocalPosition(Vector2I tile)
    {
        return new Vector2(
            0.5f * TileSet.TileSize.X * (1 + tile.Y + SizeMin.Y + tile.X + SizeMin.X),
            0.5f * TileSet.TileSize.Y * (1 + tile.Y + SizeMin.Y - tile.X - SizeMin.X)
        );
    }

    private bool IsCursorOnTile(int x, int y)
    {
        return CursorPos?.X == x && CursorPos?.Y == y;
    }

    protected bool IsPawnReady(int x, int y)
    {
        return IsPawnReady(new Vector2I(x, y));
    }

    protected bool IsPawnReady(Vector2I tile)
    {
        return GetPawnAt(tile)?.IsReady() ?? false;
    }

    // TODO just use Vector2I instead of (int, int)
    private IEnumerable<(Func<int, int, bool>, int)> GetUntargetedBitPackingInfos()
    {
        yield return (TileExists, TileExistsBitShift);
        yield return (IsCursorOnTile, CursorBitShift);
        yield return (IsPawnReady, ReadyPawnBitShift);
        yield return (SelectionState.IsTileSelected, SelectedBitShift);
        yield return (SelectionState.IsTileInActionRange, ActionRangeBitShift);
        yield return (SelectionState.CanPerformActionAtTile, ActionValidBitShift);
        yield return (SelectionState.IsTileTargeted, TargetBitShift);
        yield return (SelectionState.IsTileInEffectArea, AreaOfEffectBitShift);
        // TODO Engagements
    }

    private IEnumerable<(IEnumerable<Vector2I>, int)> GetTargetedBitPackingInfos()
    {
        yield return (SelectionState.GetPathToTarget(this, false), TilePathBitShift);
    }

    private static Color GetBitsColor(uint bits)
    {
        if (BitsColorCache.TryGetValue(bits, out Color color))
        {
            return color;
        }

        if (BitsColorCache.Count >= MaxBitsColorCacheSize)
        {
            return new Color(BitConverter.UInt32BitsToSingle(bits), 0f, 0f, 0f);
        }

        BitsColorCache[bits] = new Color(BitConverter.UInt32BitsToSingle(bits), 0f, 0f, 0f);
        return GetBitsColor(bits);
    }
}
