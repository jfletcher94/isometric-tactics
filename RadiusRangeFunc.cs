using Godot;

namespace IsometricTactics;

[GlobalClass]
public partial class RadiusRangeFunc : RangeFunc
{
    [Export] private float _min = -1f;
    [Export] private float _max = -1f;

    public RadiusRangeFunc() {}

    public RadiusRangeFunc(float min, float max)
    {
        _min = min;
        _max = max;
    }

    public override bool IsTileInRange(Vector2I fromTile, Vector2I toTile)
    {
        return (_min < 0f || CompareTileDistToRange(fromTile, toTile, _min) >= 0)
            && (_max < 0f || CompareTileDistToRange(fromTile, toTile, _max) <= 0);
    }

    private int CompareTileDistToRange(Vector2I fromTile, Vector2I toTile, float range)
    {
        return (Mathf.Pow(toTile.X - fromTile.X, 2f) + Mathf.Pow(toTile.Y - fromTile.Y, 2f))
            .CompareTo(Mathf.Pow(range, 2f));
    }
}
