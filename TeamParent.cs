using System.Collections.Generic;

using Godot;

namespace IsometricTactics;

public partial class TeamParent : Node2D
{
    [Export] public int ActionsPerTurn { get; private set; } = 3;
    [Export] public TeamType Team { get; private set; } = TeamType.None;
    [Export] public TileMapHelper LevelTileMap { get; private set; }
    public int RemainingActions { get; set; }

    private readonly SortedList<Vector2I, IPawn> _pawns = new(TileMapHelper.Vector2IComparer);

    public override void _Ready()
    {
        LevelTileMap.RegisterTeam(this);
        AdvanceTurn();
    }

    public override void _Process(double delta) {}

    public bool RegisterPawn(IPawn pawn)
    {
        return _pawns.TryAdd(pawn.Tile, pawn);
    }

    public void AdvanceTurn()
    {
        RemainingActions = ActionsPerTurn;
        foreach (IPawn pawn in GetPawns())
        {
            pawn.AdvanceTurn();
        }
    }

    public IPawn? GetPawn(Vector2I tile)
    {
        return _pawns.GetValueOrDefault(tile);
    }

    public IEnumerable<IPawn> GetPawns()
    {
        return _pawns.Values;
    }

    public bool RemovePawn(Vector2I tile, out IPawn? pawn)
    {
        return _pawns.Remove(tile, out pawn);
    }

    public bool MovePawn(Vector2I tile)
    {
        return RemovePawn(tile, out IPawn? pawn) && pawn != null && RegisterPawn(pawn);
    }
}
