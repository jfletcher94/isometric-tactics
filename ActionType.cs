namespace IsometricTactics;

public enum ActionType
{
    None = -1,
    Move,
    Attack,
    Ambush,
    Defend,
    Disengage,
    Count
}
