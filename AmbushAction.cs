using Godot;

namespace IsometricTactics;

public class AmbushAction : IPawnAction
{
    private float _timeRemaining;

    public AmbushAction(float timeRemaining)
    {
        _timeRemaining = timeRemaining;
    }

    public ActionType GetActionType()
    {
        return ActionType.Ambush;
    }

    public void Update(IPawn pawn, double delta)
    {
        _timeRemaining -= (float)delta;
    }

    public bool IsOngoing()
    {
        return _timeRemaining > 0f;
    }

    public bool OnFinished(IPawn pawn)
    {
        pawn.GetOwnedEngagement()?.StartEngagement();
        return true;
    }

    public Vector2 GetDirection()
    {
        return Vector2.Zero;
    }
}
