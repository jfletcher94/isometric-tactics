using Godot;

namespace IsometricTactics;

// A pawn action is what a pawn is doing while controls are frozen, and is a separate concept from an engagement
// TODO replace with Engagement at 'Ready' stage?
public interface IPawnAction
{
    public ActionType GetActionType();

    public void Update(IPawn pawn, double delta);

    public bool IsOngoing();

    public bool OnFinished(IPawn pawn);

    public Vector2 GetDirection();

    public static bool SelectOwnTileByDefault(ActionType actionType)
    {
        return actionType switch
        {
            ActionType.Ambush or ActionType.Defend or ActionType.Disengage => true, _ => false
        };
    }
}
