using Godot;

namespace IsometricTactics;

public partial class PawnSprite : AnimatedSprite2D
{
    private static readonly StringName[] _animationTypeNames;

    [Export] private float _selectedScalar = 2f;

    static PawnSprite()
    {
        _animationTypeNames = new StringName[(int)PawnAnimationType.Count];
        _animationTypeNames[(int)PawnAnimationType.Idle] = new StringName("idle");
        _animationTypeNames[(int)PawnAnimationType.Walk] = new StringName("walk");
        _animationTypeNames[(int)PawnAnimationType.Run] = new StringName("run");
        _animationTypeNames[(int)PawnAnimationType.Attack] = new StringName("attack");
        _animationTypeNames[(int)PawnAnimationType.Defend] = new StringName("hurt");
    }

    public static PawnAnimationType GetAnimationType(ActionType actionType)
    {
        return actionType switch
        {
            ActionType.Move => PawnAnimationType.Run,
            ActionType.Attack => PawnAnimationType.Attack,
            ActionType.Ambush => PawnAnimationType.Walk, // TODO temp
            ActionType.Defend => PawnAnimationType.Defend,
            _ => PawnAnimationType.Idle
        };
    }

    public static StringName GetAnimation(PawnAnimationType type)
    {
        return type is PawnAnimationType.None or PawnAnimationType.Count
            ? TileMapHelper.Empty
            : _animationTypeNames[(int)type];
    }

    public override void _Ready()
    {
        Animation = GetAnimation(PawnAnimationType.Idle);
        Play();
    }

    public void SetSelected(bool selected)
    {
        SpeedScale = selected && Animation == GetAnimation(PawnAnimationType.Idle)
            ? _selectedScalar
            : 1f;
    }

    public void SetAction(ActionType actionType)
    {
        SetSelected(false); // TODO super fragile?
        Animation = GetAnimation(GetAnimationType(actionType));
    }

    public void FaceDirection(Vector2? direction)
    {
        if (direction == null)
        {
            return;
        }

        FlipH = direction.Value.X == 0f ? FlipH : direction.Value.X < 0f;
    }

    /// <summary>
    /// TODO temp
    /// </summary>
    public void KnockOut()
    {
        SetAction(ActionType.None);
        Modulate = Colors.DarkGray;
        Stop();
        Frame = 1;
    }

    /// <summary>
    /// TODO temp
    /// </summary>
    public void Revive()
    {
        Modulate = Colors.White;
        Play();
    }
}
