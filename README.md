### Isometric Tactics

This project is an experiment to create a proof-of-concept turn-based tactics combat system that's not based on hitpoints. 

##### Motivation

- Hitpoint-based combat systems naturally gravitate towards DPS.
- Explore tatics gameplay space opened up by de-emphasizing DPS.
- Build a combat system focused on interactions between player-controlled characters as tool for emergent storytelling.
- Make the value of taking different actions dependent on the moment-to-moment tactical situation.
- Create uncertainty about outcomes with emergent complexity instead of randomness.

##### High-Level Design

- Turn-based tactics combat between 2 teams on a 2D grid.
- Combat is "engagement"-based—performing an action commits a character to an engagement, and they cannot perform any other action while the engagement is ongoing.
- How and when an engagement ends is largely determined by other characters intervening (or failing to intervene).
- Criteria for and consequences of winning/losing an engagement, and character stats (for the engagement's duration) depend on the particularities of the individual engagement.

##### Implementation

- [x] 2D grid with character pawns
- [x] Selection of characters, actions and tiles
- [x] Grid tile shader
- [x] Visual feedback of ongoing actions/engagements
- [x] Visual feedback of available actions
- [ ] Visual feedback of status of characters, current turn
- [ ] Event log/character memory
- [x] Commit buttons
- [x] Move, attack actions
- [x] Ambush action
- [x] Disengage action
- [ ] Defend action
- [ ] Friendly actions
- [ ] Combat context engine
