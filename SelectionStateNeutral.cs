using Godot;

namespace IsometricTactics;

public class SelectionStateNeutral : ISelectionState
{
    private readonly SelectionStateNone _selectionStateNone;
    private Vector2I _tile;

    internal SelectionStateNeutral(SelectionStateNone selectionStateNone)
    {
        _selectionStateNone = selectionStateNone;
    }

    public ISelectionState.Type StateType()
    {
        return ISelectionState.Type.NeutralType;
    }

    public ISelectionState Select(TileMapHelper tileMap, Vector2I? tile)
    {
        return _selectionStateNone.SelectHelper(tileMap, tile);
    }

    public ISelectionState Deselect()
    {
        return _selectionStateNone;
    }

    public ISelectionState Reset()
    {
        return _selectionStateNone;
    }

    public ISelectionState ChooseAction(ActionType actionType)
    {
        return this;
    }

    public Vector2I? GetSelectedTile()
    {
        return _tile;
    }

    internal ISelectionState SelectHelper(Vector2I tile)
    {
        _tile = tile;
        return this;
    }
}
