namespace IsometricTactics;

public class Engagement
{
    public Stage EngagementStage { get; private set; }
    public IPawn Owner { get; private set; }
    public IPawn Target { get; private set; }
    public ActionType ActionType { get; private set; }

    public Engagement(IPawn owner, IPawn target, ActionType actionType)
    {
        EngagementStage = Stage.Ready;
        Owner = owner;
        Target = target;
        ActionType = actionType;
    }

    public void StartEngagement()
    {
        EngagementStage = Stage.Active;
        Owner.NotifyEngagementStarted(this);
        if (Owner == Target)
        {
            return;
        }

        Target.NotifyEngagementStarted(this);
    }

    public void EndEngagement()
    {
        EngagementStage = Stage.Complete;
        Owner.NotifyEngagementEnded(this);
        if (Owner == Target)
        {
            return;
        }

        Target.NotifyEngagementEnded(this);
    }

    public void AdvanceTurn()
    {
        if (EngagementStage != Stage.Active)
        {
            return;
        }

        Target.NotifyEngagementIteration(this);
        // Previous method call can cause engagement to end
        if (EngagementStage != Stage.Active || Owner == Target)
        {
            return;
        }

        Owner.NotifyEngagementIteration(this);
    }

    public enum Stage
    {
        Ready,
        Active,
        Complete
    }
}
