using System.Collections.Generic;
using System.Linq;

using Godot;

namespace IsometricTactics;

public partial class UiParent : Control
{
    private List<ButtonPanel> _buttonPanels;

    public override void _Ready()
    {
        UpdateOffset();
//		GetViewport().SizeChanged += UpdateOffset;
        _buttonPanels = FindChildren("*", recursive: false).OfType<ButtonPanel>().ToList();
        OnUpdateSelected();
    }

    public void OnUpdateSelected()
    {
        foreach (ButtonPanel buttonPanel in _buttonPanels)
        {
            buttonPanel.OnUpdateSelected();
        }
    }

    private void UpdateOffset()
    {
        OffsetRight = GetViewport().GetVisibleRect().Size.X;
        OffsetBottom = GetViewport().GetVisibleRect().Size.Y;
    }
}
