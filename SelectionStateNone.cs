using Godot;

namespace IsometricTactics;

public class SelectionStateNone : ISelectionState
{
    private readonly ISelectionState[] _states =
        new ISelectionState[(int)ISelectionState.Type.Count];

    internal SelectionStateNone()
    {
        foreach (var state in new ISelectionState[]
            {
                this, new SelectionStateNeutral(this), new SelectionStateFriendly(this)
            })
        {
            _states[(int)state.StateType()] = state;
        }
    }

    public ISelectionState GetState(ISelectionState.Type type)
    {
        return _states[(int)type];
    }

    public ISelectionState.Type StateType()
    {
        return ISelectionState.Type.NoneType;
    }

    public ISelectionState Select(TileMapHelper tileMap, Vector2I? tile)
    {
        return SelectHelper(tileMap, tile);
    }

    public ISelectionState Deselect()
    {
        return this;
    }

    public ISelectionState Reset()
    {
        return this;
    }

    public ISelectionState ChooseAction(ActionType actionType)
    {
        return this;
    }

    public Vector2I? GetSelectedTile()
    {
        return null;
    }

    internal ISelectionState SelectHelper(TileMapHelper tileMap, Vector2I? tile)
    {
        if (!tileMap.TileExists(tile))
        {
            return this;
        }

        IPawn? pawn = tileMap.GetPawnAt(tile!);
        return (pawn?.Team ?? TeamType.None) == tileMap.ActiveTeam
            ? ((SelectionStateFriendly)GetState(ISelectionState.Type.FriendlyType)).SelectHelper(
                (Vector2I)tile!, pawn!
            )
            : ((SelectionStateNeutral)GetState(ISelectionState.Type.NeutralType)).SelectHelper(
                (Vector2I)tile!
            );
    }
}
