namespace IsometricTactics;

public enum PawnAnimationType
{
    None = -1,
    Idle,
    Walk,
    Run,
    Attack,
    Defend,
    Count
}
