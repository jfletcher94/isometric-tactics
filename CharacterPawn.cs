using System;
using System.Linq;

using Godot;

namespace IsometricTactics;

public partial class CharacterPawn : Node2D, IPawn
{
    [Export] protected int ActionsPerTurn = 2;
    [Export] private float _moveSpeed = 128f;
    [Export] private float _stationaryActionDelaySeconds = 1f;
    [Export] public int MaxFocus { get; private set; } = 40;
    [Export] private float _attackPower = 10f;
    [Export] private float _defensePower = 10f;
    [Export] private float _interruptedAttackScalar = 0.75f; // TODO narrow down per action type
    [Export] private float _interruptedDefenseScalar = 0.5f; // TODO narrow down per action type
    [Export] private float _counterAttackScalar = 0.75f;
    [Export] private float _counterDefenseScalar = 1.2f;

    [Export(PropertyHint.ResourceType, nameof(RangeFunc))]
    private RangeFunc _moveRangeFunc;

    [Export(PropertyHint.ResourceType, nameof(RangeFunc))]
    private RangeFunc _attackRangeFunc;

    [Signal] public delegate void ActionCompletedEventHandler();

    protected int RemainingActions;
    private IPawnAction? _pawnAction;
    protected Engagement? Engagement;
    protected bool Interrupted = false;
    private int _focus;

    public Node2D AsNode()
    {
        return this;
    }

    public TeamType Team => TeamParent().Team;
    public Vector2I Tile { get; private set; }

    public PawnSprite Sprite { get; private set; } = null!;

    public int AvailableActions => Math.Min(RemainingActions, TeamParent().RemainingActions);

    public int Focus
    {
        get => _focus;

        protected set
        {
            if (_focus == value)
            {
                return;
            }
            _focus = Math.Max(value, 0);
            if (_focus == 0)
            {
                KnockOut();
            }
        }
    }

    public override void _Ready()
    {
        Tile = LevelTileMap().TileAtPosition(GlobalPosition)
            ?? Fail<Vector2I>(new NullReferenceException());
        UpdatePositionFromTile();
        TeamParent().RegisterPawn(this);
        Sprite = (PawnSprite)FindChild("PawnSprite");
        Focus = MaxFocus;
    }

    public override void _Process(double delta)
    {
        if (_pawnAction == null)
        {
            return;
        }

        _pawnAction.Update(this, delta);
        if (_pawnAction.IsOngoing())
        {
            return;
        }

        // TODO this is fragile
        bool emitSignal = _pawnAction.OnFinished(this);
        _pawnAction = null; // Before emitting signal
        if (emitSignal)
        {
            EmitSignal(SignalName.ActionCompleted);
        }
        UpdateAnimation();
    }

    public bool TryMoveToTile(Vector2I tile)
    {
        return TryMoveToTile(tile, true);
    }

    protected bool TryMoveToTile(Vector2I tile, bool checkForEffectArea)
    {
        Vector2I oldTile = Tile;
        Tile = tile;
        if (!TeamParent().MovePawn(oldTile))
        {
            Tile = oldTile;
            TeamParent().RegisterPawn(this);
            return false;
        }

        if (!checkForEffectArea
            || !GetAffectingEffectAreaAtTile(Tile, out IPawn? pawn)
            || pawn == null)
        {
            return true;
        }

        StringName signalName = pawn.ActivateReaction(this);
        if (signalName == TileMapHelper.Empty)
        {
            EmitSignal(SignalName.ActionCompleted);
        }
        else
        {
            pawn.Connect(
                signalName, Callable.From(() => EmitSignal(SignalName.ActionCompleted)),
                (uint)ConnectFlags.OneShot
            );
        }
        return true;
    }

    public TileMapHelper LevelTileMap()
    {
        return TeamParent().LevelTileMap;
    }

    public TeamParent TeamParent()
    {
        return GetParent<TeamParent>();
    }

    public float GetAttackPower(bool isAttackOwner)
    {
        return _attackPower
            * (Interrupted ? _interruptedAttackScalar : 1f)
            * (isAttackOwner ? 1f : _counterAttackScalar);
    }

    public float GetDefensePower(bool isAttackOwner)
    {
        return _defensePower
            * (Interrupted ? _interruptedDefenseScalar : 1f)
            * (isAttackOwner ? _counterDefenseScalar : 1f);
    }

    public Engagement? GetOwnedEngagement()
    {
        return Engagement;
    }

    public virtual bool IsEngaged()
    {
        return IsEngagedAsOwner() || IsEngagedAsTarget();
    }

    public virtual bool IsEngagedAsTarget()
    {
        return ((IPawn)this).GetEngagementsAsTarget().Any();
    }

    public virtual bool IsEngagedAsOwner()
    {
        return GetOwnedEngagement() != null;
    }

    public void NotifyEngagementStarted(Engagement engagement)
    {
        if (Engagement != null && engagement.Owner != this)
        {
            Engagement.EndEngagement();
            Engagement = null;
            Interrupt();
        }
        UpdateAnimation();
    }

    public void NotifyEngagementEnded(Engagement engagement)
    {
        if (engagement.Owner == this)
        {
            Engagement = null;
        }
        UpdateAnimation();
    }

    public void NotifyEngagementIteration(Engagement engagement)
    {
        if (engagement.ActionType != ActionType.Attack)
        {
            return;
        }

        if (engagement.Owner == this)
        {
            if (engagement.Target.CanCounterActionFromTile(ActionType.Attack, Tile))
            {
                Focus -= Mathf.CeilToInt(
                    TileMapHelper.BaseFocusDamageMultiplier
                    * engagement.Target.GetAttackPower(false)
                    / GetDefensePower(true)
                );
            }
        }
        else
        {
            Focus -= Mathf.CeilToInt(
                TileMapHelper.BaseFocusDamageMultiplier
                * engagement.Owner.GetAttackPower(true)
                / GetDefensePower(false)
            );
        }
    }

    public void AdvanceTurn()
    {
        Engagement?.AdvanceTurn();
        RemainingActions = ActionsPerTurn;
        if (RemainingActions == 0)
        {
            return; // TODO hacky
        }

        // TODO test
        if (!IsEngaged())
        {
            Interrupted = false;
            Focus = MaxFocus; // TODO focus recovery?
        }
    }

    public StringName ActivateReaction(IPawn pawn)
    {
        if (Engagement?.ActionType != ActionType.Ambush)
        {
            return TileMapHelper.Empty;
        }

        Engagement.EndEngagement();
        return PerformActionAtTile(ActionType.Attack, pawn.Tile, 0);
    }

    public bool IsTileInEffectArea(ActionType actionType, Vector2I? target, Vector2I tile)
    {
        return target != null
            && actionType == ActionType.Ambush
            && _attackRangeFunc.IsTileInRange((Vector2I)target, tile);
    }

    public bool CanCounterActionFromTile(ActionType actionType, Vector2I tile)
    {
        return actionType == ActionType.Attack && _attackRangeFunc.IsTileInRange(Tile, tile);
    }

    public bool CanPerformAction(ActionType actionType)
    {
        return _pawnAction == null
            && AvailableActions > 0
            && actionType switch
            {
                ActionType.None or ActionType.Count => false,
                ActionType.Disengage => IsEngagedAsOwner(),
                _ => !IsEngaged()
            };
    }

    public bool IsTileInActionRange(ActionType actionType, Vector2I tile)
    {
        return CanPerformAction(actionType)
            && actionType switch
            {
                ActionType.Move => _moveRangeFunc.IsTileInRange(Tile, tile),
                ActionType.Attack => _attackRangeFunc.IsTileInRange(Tile, tile),
                ActionType.Ambush or ActionType.Defend or ActionType.Disengage => Tile.Equals(tile),
                _ => true // invalid types ruled out in CanPerformAction
            };
    }

    public bool CanPerformActionAtTile(ActionType actionType, Vector2I tile)
    {
        return IsTileInActionRange(actionType, tile) // includes call to CanPerformAction
            && actionType switch
            {
                ActionType.Move => LevelTileMap().GetPawnAt(tile) == null,
                ActionType.Attack => ((IPawn)this).IsEnemy(LevelTileMap().GetPawnAt(tile)),
                _ => true
            };
    }

    public StringName PerformActionAtTile(ActionType actionType, Vector2I tile, int actionCost = 1)
    {
        if (!((IPawn)this).CanPerformActionAtTile(actionType, tile))
        {
            return TileMapHelper.Empty;
        }

        if ((_pawnAction = PerformActionAtTileHelper(actionType, tile)) == null)
        {
            return TileMapHelper.Empty;
        }

        RemainingActions -= actionCost;
        TeamParent().RemainingActions -= actionCost;
        UpdateAnimation();
        return SignalName.ActionCompleted;
    }

    public bool GetAffectingEffectAreaAtTile(Vector2I tile, out IPawn? result)
    {
        result = LevelTileMap()
            .GetAllPawns()
            .FirstOrDefault(
                pawn => ((IPawn)this).IsEnemy(pawn)
                    && pawn.GetOwnedEngagement()?.ActionType == ActionType.Ambush
                    && pawn.IsTileInEffectArea(
                        ActionType.Ambush, pawn.GetOwnedEngagement()?.Target.Tile, tile
                    )
            );
        return result != null;
    }

    public void MoveByIncrement(Vector2 delta)
    {
        Position += delta;
    }

    public void UpdatePositionFromTile()
    {
        GlobalPosition = LevelTileMap().TileToGlobalPosition(Tile) ?? GlobalPosition;
    }

    public void Interrupt()
    {
        Interrupted = true;
    }

    public void KnockOut()
    {
        GetOwnedEngagement()?.EndEngagement();
        foreach (Engagement engagement in ((IPawn)this).GetEngagementsAsTarget())
        {
            engagement.EndEngagement();
        }
        Sprite.KnockOut();
        Focus = 0;
        RemainingActions = 0;
        ActionsPerTurn = 0;
    }

    private IPawnAction? PerformActionAtTileHelper(ActionType actionType, Vector2I tile)
    {
        return actionType switch
        {
            ActionType.Move => MoveToTile(tile),
            ActionType.Attack => AttackTile(tile),
            ActionType.Ambush => AmbushFromTile(tile),
            ActionType.Disengage => DisengageFromTile(tile),
            _ => null
        };
    }

    private IPawnAction? MoveToTile(Vector2I tile)
    {
        Vector2I oldTile = Tile;
        return TryMoveToTile(tile, false) ? new MoveAction(this, oldTile, Tile, _moveSpeed) : null;
    }

    private IPawnAction? AttackTile(Vector2I tile)
    {
        Engagement = new Engagement(this, LevelTileMap().GetPawnAt(tile)!, ActionType.Attack);
        return new AttackAction(
            (Vector2)LevelTileMap().GetFromTileToTile(Tile, tile)!, _stationaryActionDelaySeconds
        );
    }

    private IPawnAction? AmbushFromTile(Vector2I tile)
    {
        if (tile != Tile)
        {
            return null;
        }

        Engagement = new Engagement(this, this, ActionType.Ambush);
        return new AmbushAction(_stationaryActionDelaySeconds);
    }

    private IPawnAction? DisengageFromTile(Vector2I tile)
    {
        return tile != Tile ? null : new DisengageAction(_stationaryActionDelaySeconds);
    }

    private void UpdateAnimation()
    {
        if (_pawnAction != null)
        {
            Sprite.SetAction(_pawnAction.GetActionType());
            Sprite.FaceDirection(_pawnAction.GetDirection());
            return;
        }

        if (Engagement != null)
        {
            Sprite.SetAction(Engagement.ActionType);
            Sprite.FaceDirection(LevelTileMap().GetFromTileToTile(Tile, Engagement.Target.Tile));
            return;
        }

        Engagement? engagement = LevelTileMap()
            .GetAllPawns()
            .Select(pawn => pawn.GetOwnedEngagement())
            .FirstOrDefault(engagement => engagement?.Target == this);
        if (engagement != null)
        {
            Sprite.SetAction(ActionType.Defend);
            Sprite.FaceDirection(LevelTileMap().GetFromTileToTile(Tile, engagement.Owner.Tile));
            return;
        }

        Sprite.SetAction(ActionType.None);
    }

    private static T Fail<T>(Exception e)
    {
        throw e;
    }
}
