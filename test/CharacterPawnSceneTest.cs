using System;
using System.Collections.Generic;
using System.Linq;

using GD_NET_ScOUT;

using Godot;

namespace IsometricTactics.test;

[Test]
public partial class CharacterPawnSceneTest : CharacterPawn
{

    private Engagement FakeEngagement => new(this, this, ActionType.None);
    private Vector2I _startingTile;
    private Vector2I _validMoveTile;
    private CharacterPawn _pawnPlayer = null!;
    private CharacterPawn _pawnCpu = null!;
    private readonly List<Action> _afterEachActions = new();

    [BeforeAll]
    public void BeforeAll()
    {
        _startingTile = Tile;
        _validMoveTile = _startingTile + new Vector2I(2, 2);
        foreach (IPawn pawn in LevelTileMap().GetAllPawns())
        {
            if (this == pawn || pawn is not CharacterPawn characterPawn)
            {
                continue;
            }

            switch (pawn.Team)
            {
                case TeamType.Player:
                    _pawnPlayer = characterPawn;
                    break;
                case TeamType.Cpu:
                    _pawnCpu = characterPawn;
                    break;
            }
        }
        Assert.IsNotNull(_pawnPlayer);
        Assert.IsNotNull(_pawnCpu);
    }

    [BeforeEach] public void BeforeEach() {}

    [AfterEach]
    public void AfterEach()
    {
        _afterEachActions.Reverse();
        foreach (Action action in _afterEachActions)
        {
            action.Invoke();
        }
        _afterEachActions.Clear();
        Assert.IsTrue(Tile == _startingTile || TryMoveToTile(_startingTile, false));
        foreach (IPawn pawn in LevelTileMap().GetAllPawns())
        {
            pawn.GetOwnedEngagement()?.EndEngagement();
            pawn.UpdatePositionFromTile();
        }
        AdvanceTurnToTeam(Team, true);
    }

    [Test]
    public void TestCanPerformActionAtTile_ActionConflict()
    {
        StringName signalName = PerformActionAtTile(ActionType.Move, _validMoveTile);
        Assert.AreNotEqual(signalName, TileMapHelper.Empty);
        Assert.IsFalse(CanPerformActionAtTile(ActionType.Move, _startingTile + Vector2I.One));
        this.GetTestRunner()
            .WaitForSignal(
                this, signalName,
                () => Assert.IsTrue(
                    CanPerformActionAtTile(ActionType.Move, _startingTile + Vector2I.One)
                )
            );
    }

    [Test]
    public void TestCanPerformActionAtTile_Move_Engaged()
    {
        TestCanPerformActionAtTileHelper_Engaged(ActionType.Move, _validMoveTile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Attack_Engaged()
    {
        TestCanPerformActionAtTileHelper_Engaged(ActionType.Attack, _pawnCpu.Tile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Ambush_Engaged()
    {
        TestCanPerformActionAtTileHelper_Engaged(ActionType.Ambush, Tile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Disengage_Engaged()
    {
        TestCanPerformActionAtTileHelper_Engaged(ActionType.Disengage, Tile);
    }

    private void TestCanPerformActionAtTileHelper_Engaged(ActionType actionType, Vector2I tile)
    {
        Engagement = FakeEngagement;
        if (actionType == ActionType.Disengage)
        {
            Assert.IsTrue(CanPerformActionAtTile(actionType, tile));
        }
        else
        {
            Assert.IsFalse(CanPerformActionAtTile(actionType, tile));
        }
        StringName signalName = _pawnCpu.PerformActionAtTile(ActionType.Attack, Tile);
        Assert.AreNotEqual(signalName, TileMapHelper.Empty);
        this.GetTestRunner()
            .WaitForSignal(
                _pawnCpu, signalName, () => Assert.IsFalse(CanPerformActionAtTile(actionType, tile))
            );
    }

    [Test]
    public void TestCanPerformActionAtTile_Move_InvalidTile()
    {
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Move, Tile);
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Move, _pawnPlayer.Tile);
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Move, _pawnCpu.Tile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Attack_InvalidTile()
    {
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Attack, Tile);
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Attack, _pawnPlayer.Tile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Ambush_InvalidTile()
    {
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Ambush, _pawnPlayer.Tile);
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Ambush, _pawnCpu.Tile);
    }

    [Test]
    public void TestCanPerformActionAtTile_Disengage_InvalidTile()
    {
        Engagement = FakeEngagement;
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Disengage, _pawnPlayer.Tile);
        TestCanPerformActionAtTileHelper_InvalidTile(ActionType.Disengage, _pawnCpu.Tile);
    }

    private void TestCanPerformActionAtTileHelper_InvalidTile(
        ActionType actionType, params Vector2I[] tiles)
    {
        Assert.IsFalse(CanPerformActionAtTile(actionType, -Vector2I.One)); // Non-existent tile
        Assert.IsFalse(CanPerformActionAtTile(actionType, Vector2I.Zero)); // Out-of-range tile
        foreach (Vector2I tile in tiles)
        {
            Assert.IsFalse(CanPerformActionAtTile(actionType, tile));
        }
    }

    [Test]
    public void TestCanPerformActionAtTile_Move()
    {
        Assert.IsTrue(CanPerformActionAtTile(ActionType.Move, _validMoveTile));
    }

    [Test]
    public void TestCanPerformActionAtTile_Attack()
    {
        Assert.IsTrue(CanPerformActionAtTile(ActionType.Attack, _pawnCpu.Tile));
    }

    [Test]
    public void TestCanPerformActionAtTile_Ambush()
    {
        Assert.IsTrue(CanPerformActionAtTile(ActionType.Ambush, Tile));
    }

    [Test]
    public void TestCanPerformActionAtTile_Disengage()
    {
        Engagement = FakeEngagement;
        Assert.IsTrue(CanPerformActionAtTile(ActionType.Disengage, Tile));
    }

    [Test]
    public void TestPerformActionAtTile_Move()
    {
        TestPerformActionAtTileHelper(ActionType.Move, _validMoveTile, null, true);
    }

    [Test]
    public void TestPerformActionAtTile_Attack()
    {
        TestPerformActionAtTileHelper(ActionType.Attack, _pawnCpu.Tile, _pawnCpu);
    }

    [Test]
    public void TestPerformActionAtTile_Ambush()
    {
        TestPerformActionAtTileHelper(ActionType.Ambush, Tile, this);
    }

    [Test]
    public void TestPerformActionAtTile_Disengage()
    {
        Engagement = new Engagement(this, this, ActionType.None);
        TestPerformActionAtTileHelper(ActionType.Disengage, Tile);
    }

    private void TestPerformActionAtTileHelper(
        ActionType actionType, Vector2I tile, IPawn? engagementTarget = null, bool moved = false)
    {
        int prevRemainingActions = RemainingActions;
        int prevParentRemainingActions = TeamParent().RemainingActions;
        int prevAvailableActions = AvailableActions;
        StringName signalName = PerformActionAtTile(actionType, tile);
        Assert.AreNotEqual(signalName, TileMapHelper.Empty);
        Assert.AreEqual(RemainingActions, prevRemainingActions - 1);
        Assert.AreEqual(TeamParent().RemainingActions, prevParentRemainingActions - 1);
        Assert.AreEqual(AvailableActions, prevAvailableActions - 1);
        this.GetTestRunner()
            .WaitForSignal(
                this, signalName, () =>
                {
                    if (moved)
                    {
                        Assert.AreEqual(Tile, _validMoveTile);
                        Assert.AreEqual(Tile, LevelTileMap().TileAtPosition(GlobalPosition));
                    }
                    if (engagementTarget == null)
                    {
                        Assert.IsFalse(IsEngaged());
                    }
                    else
                    {
                        Assert.IsTrue(IsEngagedAsOwner());
                        Assert.AreEqual(engagementTarget, GetOwnedEngagement()?.Target);
                    }
                }
            );
    }

    [Test]
    public void TestActionInterrupted_Move_Ambush()
    {
        Vector2I pawnCpuStartingTile = _pawnCpu.Tile;
        _afterEachActions.Add(() => Assert.IsTrue(_pawnCpu.TryMoveToTile(pawnCpuStartingTile)));
        Assert.IsTrue(_pawnCpu.TryMoveToTile(_pawnCpu.Tile + Vector2I.One));
        StringName signalName = _pawnCpu.PerformActionAtTile(ActionType.Ambush, _pawnCpu.Tile);
        Assert.AreNotEqual(signalName, TileMapHelper.Empty);
        this.GetTestRunner()
            .WaitForSignal(
                _pawnCpu, signalName, () =>
                {
                    Assert.IsFalse(Interrupted);
                    signalName = PerformActionAtTile(ActionType.Move, _validMoveTile);
                    Assert.AreNotEqual(signalName, TileMapHelper.Empty);
                    this.GetTestRunner()
                        .WaitForSignal(
                            this, signalName, () =>
                            {
                                Assert.AreEqual(Tile, _startingTile + Vector2I.One);
                                Assert.AreEqual(
                                    _pawnCpu.GetOwnedEngagement(),
                                    ((IPawn)this).GetEngagementsAsTarget().First()
                                );
                                Assert.AreEqual(
                                    _pawnCpu.GetOwnedEngagement()?.ActionType, ActionType.Attack
                                );
                                Assert.AreEqual(_pawnCpu.GetOwnedEngagement()?.Target, this);
                                Assert.IsTrue(Interrupted);
                            }
                        );
                }
            );
    }

    [Test]
    public void TestActivateReaction_Ambush()
    {
        Vector2I pawnCpuStartingTile = _pawnCpu.Tile;
        _afterEachActions.Add(() => Assert.IsTrue(_pawnCpu.TryMoveToTile(pawnCpuStartingTile)));
        Assert.IsTrue(IsTileInEffectArea(ActionType.Ambush, Tile, _pawnCpu.Tile));
        StringName signalName = PerformActionAtTile(ActionType.Ambush, Tile);
        Assert.AreNotEqual(signalName, TileMapHelper.Empty);
        this.GetTestRunner()
            .WaitForSignal(
                this, signalName, () =>
                {
                    AdvanceTurnToTeam(_pawnCpu.Team);
                    signalName = _pawnCpu.PerformActionAtTile(
                        ActionType.Move, _pawnCpu.Tile + Vector2I.One
                    );
                    Assert.AreNotEqual(signalName, TileMapHelper.Empty);
                    this.GetTestRunner()
                        .WaitForSignal(
                            _pawnCpu, signalName, () =>
                            {
                                AdvanceTurnToTeam(Team);
                                Assert.AreEqual(RemainingActions, ActionsPerTurn);
                                Assert.AreEqual(
                                    TeamParent().RemainingActions, TeamParent().ActionsPerTurn
                                );
                                Assert.IsTrue(IsEngagedAsOwner());
                                Assert.AreEqual(
                                    GetOwnedEngagement()!.ActionType, ActionType.Attack
                                );
                                Assert.AreEqual(GetOwnedEngagement()!.Owner, this);
                                Assert.AreEqual(GetOwnedEngagement()!.Target, _pawnCpu);
                                signalName = PerformActionAtTile(ActionType.Disengage, Tile);
                                Assert.AreNotEqual(signalName, TileMapHelper.Empty);
                                this.GetTestRunner()
                                    .WaitForSignal(
                                        this, signalName, () =>
                                        {
                                            Assert.AreEqual(RemainingActions, ActionsPerTurn - 1);
                                            Assert.IsFalse(IsEngaged());
                                        }
                                    );
                            }
                        );
                }
            );
    }

    [Test]
    public void TestAdvanceTurn_AvailableActions()
    {
        RemainingActions = 0;
        Assert.AreEqual(AvailableActions, 0);
        AdvanceTurnToTeam(Team, true);
        Assert.IsTrue(AvailableActions > 0);
        TeamParent().RemainingActions = 0;
        Assert.AreEqual(AvailableActions, 0);
        AdvanceTurnToTeam(Team, true);
        Assert.IsTrue(AvailableActions > 0);
    }

    [Test]
    public void TestAdvanceTurn_Interrupted()
    {
        Assert.IsFalse(Interrupted);
        Interrupt();
        Assert.IsFalse(IsEngaged());
        AdvanceTurnToTeam(Team, true);
        Assert.IsFalse(Interrupted);
        Interrupt();
        Engagement = FakeEngagement;
        Assert.IsTrue(IsEngaged());
        AdvanceTurnToTeam(Team, true);
        Assert.IsTrue(Interrupted);
    }

    [Test]
    public void TestAdvanceTurn_Focus()
    {
        Assert.AreEqual(Focus, MaxFocus);
        Focus = MaxFocus / 2;
        AdvanceTurnToTeam(Team, true);
        Assert.AreEqual(Focus, MaxFocus);
        Focus = MaxFocus / 2;
        Engagement = FakeEngagement;
        AdvanceTurnToTeam(Team, true);
        Assert.AreEqual(Focus, MaxFocus / 2);
    }

    [Test]
    public void TestKnockOut()
    {
        int actionsPerTurn = ActionsPerTurn;
        _afterEachActions.Add(() => ActionsPerTurn = actionsPerTurn);
        _afterEachActions.Add(Sprite.Revive);
        Engagement = FakeEngagement;
        Focus = -1;
        Assert.IsNull(GetOwnedEngagement());
        AdvanceTurnToTeam(Team, true);
        Assert.AreEqual(0, Focus);
        Assert.AreEqual(0, AvailableActions);
    }

    [Test]
    public void TestIsReady()
    {
        AdvanceTurnToTeam(_pawnCpu.Team);
        Assert.IsFalse(((IPawn)this).IsReady());
        AdvanceTurnToTeam(Team);
        Assert.IsTrue(((IPawn)this).IsReady());
        RemainingActions = 0;
        Assert.IsFalse(((IPawn)this).IsReady());
    }

    private bool AdvanceTurnToTeam(TeamType team, bool force = false)
    {
        if (force)
        {
            LevelTileMap().AdvanceTurn();
        }
        TeamType current = LevelTileMap().ActiveTeam;
        bool turnAdvanced = false;
        while (LevelTileMap().ActiveTeam != team)
        {
            turnAdvanced = true;
            LevelTileMap().AdvanceTurn();
            if (LevelTileMap().ActiveTeam == current)
            {
                Assert.Fail($"Team {team} not present in level");
            }
        }
        return turnAdvanced;
    }
}
