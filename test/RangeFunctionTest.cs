using GD_NET_ScOUT;

using Godot;

namespace IsometricTactics.test;

[Test]
public partial class RangeFunctionTest : Node2D
{
    private static readonly (Vector2I, Vector2I)[] CompositeComparisonVectors =
    {
        (Vector2I.Zero, Vector2I.Zero),
        (Vector2I.Zero, Vector2I.Right),
        (Vector2I.Zero, Vector2I.Right * 3),
        (Vector2I.Zero, Vector2I.Right * 5),
        (Vector2I.Zero, Vector2I.One), // dist = sqrt(2)
        (Vector2I.Zero, Vector2I.One * 2), // dist = 2*sqrt(2)
        (Vector2I.Zero, Vector2I.One * 4) // dist = 4*sqrt(2)
    };

    [Test]
    public void TestRadius_NoMin()
    {
        TestRadius_Helper(-1f, 1f, true, true, false);
    }

    [Test]
    public void TestRadius_NoMax()
    {
        TestRadius_Helper(1f, -1f, false, true, true);
    }

    [Test]
    public void TestRadius_ZeroMin()
    {
        TestRadius_Helper(0f, -1f, true, true, true);
    }

    [Test]
    public void TestRadius_ZeroMax()
    {
        TestRadius_Helper(-1f, 0f, true, false, false);
    }

    [Test]
    public void TestRadius_Exact()
    {
        TestRadius_Helper(1f, 1f, false, true, false);
    }

    private void TestRadius_Helper(float min, float max, bool zero, bool one, bool sqrt2)
    {
        RangeFunc rangeFunc = new RadiusRangeFunc(min, max);
        Assert.IsTrue(zero == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Zero));
        Assert.IsTrue(one == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Right));
        Assert.IsTrue(sqrt2 == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.One));
    }

    [Test]
    public void TestCardinal_Cardinals()
    {
        TestCardinal_Helper(true, false);
    }

    [Test]
    public void TestCardinal_Diagonals()
    {
        TestCardinal_Helper(false, true);
    }

    [Test]
    public void TestCardinal_Both()
    {
        TestCardinal_Helper(true, true);
    }

    [Test]
    public void TestCardinal_Neither()
    {
        TestCardinal_Helper(false, false);
    }

    private void TestCardinal_Helper(bool cardinals, bool diagonals)
    {
        RangeFunc rangeFunc = new CardinalDirectionsRangFunc(cardinals, diagonals);

        Assert.IsTrue(cardinals == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Right));
        Assert.IsTrue(cardinals == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Up));
        Assert.IsTrue(cardinals == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Left));
        Assert.IsTrue(cardinals == rangeFunc.IsTileInRange(Vector2I.Zero, Vector2I.Down));

        Assert.IsTrue(diagonals == rangeFunc.IsTileInRange(Vector2I.Up, Vector2I.Right));
        Assert.IsTrue(diagonals == rangeFunc.IsTileInRange(Vector2I.Left, Vector2I.Up));
        Assert.IsTrue(diagonals == rangeFunc.IsTileInRange(Vector2I.Down, Vector2I.Left));
        Assert.IsTrue(diagonals == rangeFunc.IsTileInRange(Vector2I.Right, Vector2I.Down));
    }

    [Test]
    public void TestComposite_AlwaysTrue_All()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(CompositeRangeFunc.Operator.All), uint.MaxValue
        );
    }

    [Test]
    public void TestComposite_AlwaysFalse_Any()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(CompositeRangeFunc.Operator.Any), uint.MinValue
        );
    }

    [Test]
    public void TestComposite_AlwaysTrue_None()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(CompositeRangeFunc.Operator.None), uint.MaxValue
        );
    }

    [Test]
    public void TestComposite_All()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.All, new RadiusRangeFunc(2f, 4f),
                new CardinalDirectionsRangFunc()
            ), 0b0000100u
        );
    }

    [Test]
    public void TestComposite_NotAll()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.None,
                new CompositeRangeFunc(
                    CompositeRangeFunc.Operator.All, new RadiusRangeFunc(2f, 4f),
                    new CardinalDirectionsRangFunc()
                )
            ), 0b1111011u
        );
    }

    [Test]
    public void TestComposite_Any()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.Any, new RadiusRangeFunc(2f, 4f),
                new CardinalDirectionsRangFunc()
            ), 0b0101111u
        );
    }

    [Test]
    public void TestComposite_None()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.None, new RadiusRangeFunc(2f, 4f),
                new CardinalDirectionsRangFunc()
            ), 0b1010000u
        );
    }

    [Test]
    public void TestComposite_Complex1()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.All, new RadiusRangeFunc(2f, 4f),
                new CompositeRangeFunc(
                    CompositeRangeFunc.Operator.None, new CardinalDirectionsRangFunc()
                )
            ), 0b0100000u
        );
    }

    [Test]
    public void TestComposite_Complex2()
    {
        TestComposite_Helper(
            new CompositeRangeFunc(
                CompositeRangeFunc.Operator.All, new CardinalDirectionsRangFunc(false, true),
                new CompositeRangeFunc(
                    CompositeRangeFunc.Operator.None, new RadiusRangeFunc(2f, 4f)
                )
            ), 0b1010001u
        );
    }

    /// <summary>
    /// The bit in the ones place of bitMask corresponds to element 0 in CompositeComparisonVectors, etc.
    /// </summary>
    private void TestComposite_Helper(RangeFunc rangeFunc, uint bitMask)
    {
        for (int i = 0; i < CompositeComparisonVectors.Length; i++)
        {
            Assert.IsTrue(
                ((1u & (bitMask >> i)) != 0u)
                == rangeFunc.IsTileInRange(
                    CompositeComparisonVectors[i].Item1, CompositeComparisonVectors[i].Item2
                ), $"{(1u & (bitMask >> i)) != 0u}: {CompositeComparisonVectors[i]}"
            );
        }
    }
}
