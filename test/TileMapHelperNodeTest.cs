using System;
using System.Collections.Generic;
using System.Linq;

using GD_NET_ScOUT;

using Godot;

// ReSharper disable MemberCanBePrivate.Global

namespace IsometricTactics.test;

[Test]
public partial class TileMapHelperNodeTest : TileMapHelper
{
    // Tiles need to be kept up to date with scene
    private static readonly Vector2I IsolatedTile = new(0, 16);
    private static readonly Vector2I EmptyTile = new(12, 6);

    private static readonly StringName CheckerboardScalarName = new("checkerboard_scalar");

    private static readonly StringName SelectedTileInvalidScalarName =
        new("selected_tile_invalid_scalar");

    private static readonly StringName SelectedTileColorName = new("selected_tile_color");
    private static readonly StringName SelectedTilePulseName = new("selected_tile_pulse");
    private static readonly StringName ReadyTileColorName = new("ready_tile_color");
    private static readonly StringName ReadyTilePulseName = new("ready_tile_pulse");
    private static readonly StringName ActionRangeTileColorName = new("action_range_tile_color");

    private static readonly StringName ActionRangeInvalidScalarName =
        new("action_range_invalid_scalar");

    private static readonly StringName ActionRangeTilePulseName = new("action_range_tile_pulse");
    private static readonly StringName TargetTileColorName = new("target_tile_color");
    private static readonly StringName TargetTilePulseName = new("target_tile_pulse");
    private static readonly StringName TilePathScalarName = new("tile_path_scalar");
    private static readonly StringName AoeTileColorName = new("aoe_tile_color");
    private static readonly StringName AoeTilePulseName = new("aoe_tile_pulse");

    private bool _isTesting = false;
    private bool _usingSoftwareRendering = false;
    private IPawn _pawnPlayer;
    private IPawn _pawnCpu;
    private List<Action> _afterEachActions;

    public override void _Ready()
    {
        _pawnPlayer = GetTeamParent(TeamType.Player)!.GetChildren().OfType<IPawn>().First();
        _pawnCpu = GetTeamParent(TeamType.Cpu)!.GetChildren().OfType<IPawn>().First();
    }

    public override void _UnhandledInput(InputEvent @event)
    {
        if (_isTesting)
        {
            if (@event is MockInputEventMouseButton or MockInputEventMouseMotion)
            {
                base._UnhandledInput(@event);
            }
            return;
        }

        base._UnhandledInput(@event);
        if (@event is not InputEventMouseButton
            {
                Pressed: true, ButtonIndex: MouseButton.Left
            } mouseEvent)
        {
            return;
        }

        Vector2I tile =
            LocalPositionToTileCoordinates(GlobalPositionToLocalPosition(mouseEvent.Position));
        this.GetTestRunner().Print(TileExists(tile) ? $"{tile}" : $"_{tile}");
    }

    [BeforeAll]
    public void BeforeAll()
    {
        _isTesting = true;
        _pawnPlayer.Sprite.Visible = false;
        _pawnCpu.Sprite.Visible = false;
        _afterEachActions = new List<Action>();
    }

    [AfterAll]
    public void AfterAll()
    {
        _pawnPlayer.Sprite.Visible = true;
        _pawnCpu.Sprite.Visible = true;
        TileBitsDirty = true;
        _isTesting = false;
    }

    [BeforeEach]
    public void BeforeEach()
    {
        AcceptingInput = true;
        CursorPos = null;
        LastSelectedPawn = null;
        TileBitsDirty = true;
    }

    [AfterEach]
    public void AfterEach()
    {
        _afterEachActions.Reverse();
        foreach (Action action in _afterEachActions)
        {
            action.Invoke();
        }
        _afterEachActions.Clear();
        AdvanceTurnToTeam(TeamType.Player);
        SelectionState = SelectionState.Reset();
    }

    [Test]
    public void TestGetAllTiles()
    {
        int tilesCount = 0;
        foreach (Vector2I tile in GetAllTiles())
        {
            Assert.IsTrue(TileExists(tile));
            tilesCount++;
        }
        Assert.AreEqual(GetUsedCells(0).Count, tilesCount);
    }

    [Test]
    public void TestRegisterTeam()
    {
        TeamParent? teamParent = null;
        for (TeamType i = 0; i < TeamType.Count; i++)
        {
            teamParent = GetTeamParent(i);
            if (teamParent != null)
            {
                break;
            }
        }
        Assert.IsNotNull(teamParent);
        Assert.Throws<Exception>(() => RegisterTeam(teamParent!));
    }

    [Test]
    public void TestTileExists_allExistingTiles()
    {
        foreach (Vector2I cell in GetUsedCells(0))
        {
            Assert.IsTrue(TileExists(cell - SizeMin));
        }
    }

    [Test]
    public void TestTileExists_NonExistentTiles()
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                Vector2I tile = new Vector2I(i, j);
                Assert.IsTrue(tile == IsolatedTile || !TileExists(tile - SizeMin));
            }
        }
    }

    [Test]
    public void TestIsPawnReady()
    {
        Assert.IsTrue(IsPawnReady(_pawnPlayer.Tile));
        Assert.IsFalse(IsPawnReady(Vector2I.Zero));
        Assert.IsFalse(IsPawnReady(_pawnCpu.Tile));
        AdvanceTurnToTeam(_pawnCpu.Team);
        Assert.IsFalse(IsPawnReady(_pawnPlayer.Tile));
        Assert.IsFalse(IsPawnReady(Vector2I.Zero));
        Assert.IsTrue(IsPawnReady(_pawnCpu.Tile));
    }

    [Skip("TODO: get test working")]
    [Test]
    public void TestOnCommitButtonPressed()
    {
        ClickOnTile(_pawnPlayer.Tile);
        OnActionButtonPressed(ActionType.Move);
        ClickOnTile(_pawnPlayer.Tile + Vector2I.Left * 4);
        Assert.AreEqual(_pawnPlayer.Tile + Vector2I.Left * 4, SelectionState.GetSelectedTile());
        OnCommitButtonPressed();
        Assert.IsFalse(AcceptingInput);
        this.GetTestRunner()
            .WaitForSignal(
                this, TileMapHelper.SignalName.UpdateSelected, () =>
                {
                    Assert.IsTrue(AcceptingInput);
                    Assert.AreEqual(_pawnPlayer.Tile, SelectionState.GetSelectedTile());
                    OnActionButtonPressed(ActionType.Move);
                    ClickOnTile(_pawnPlayer.Tile + Vector2I.Right * 4);
                    Assert.AreEqual(
                        _pawnPlayer.Tile + Vector2I.Right * 4, SelectionState.GetSelectedTile()
                    );
                    OnCommitButtonPressed();
                    Assert.IsFalse(AcceptingInput);
                    this.GetTestRunner()
                        .WaitForSignal(
                            this, TileMapHelper.SignalName.UpdateSelected, () =>
                            {
                                Assert.IsTrue(AcceptingInput);
                                Assert.IsNull(SelectionState.GetSelectedTile());
                            }
                        );
                }
            );
    }

    private void AdvanceTurnToTeam(TeamType team, bool force = false)
    {
        if (force)
        {
            AdvanceTurn();
        }
        TeamType current = ActiveTeam;
        while (ActiveTeam != team)
        {
            AdvanceTurn();
            if (ActiveTeam == current)
            {
                Assert.Fail($"Team {team} not present in level");
            }
        }
    }

    private void ClickOnTile(Vector2I tile)
    {
        _UnhandledInput(
            new MockInputEventMouseButton
            {
                Pressed = true,
                ButtonIndex = MouseButton.Left,
                Position = GlobalPositionToViewportPosition(
                    LocalPositionToGlobalPosition(TileCoordinatesToLocalPosition(tile))
                )
            }
        );
    }

    // Shader tests
    [Test]
    public void TestShader_UniformsNotEmpty()
    {
        Color actual = ShaderMaterial.GetShaderParameter(SelectedTileColorName).AsColor();
        Assert.AreNotEqual(Colors.Black, actual);
    }
    
    [Test]
    public void TestShader_Checkerboard()
    {
        SetShaderParameter(CheckerboardScalarName, 2f);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    bool startLight = Equals(GetColorAtTile(EmptyTile), Colors.White);
                    for (int i = -1; i <= 1; i++)
                    {
                        for (int j = -1; j <= 1; j++)
                        {
                            Color color =
                                GetColorAtTile(new Vector2I(EmptyTile.X + i, EmptyTile.Y + j));
                            if (startLight == ((i + j) % 2 == 0))
                            {
                                Assert.AreEqual(color, Colors.White);
                            }
                            else
                            {
                                Assert.AreEqual(color, Colors.Black);
                            }
                        }
                    }
                }
            );
    }

    [Test]
    public void TestShader_ReadyTile()
    {
        TestShaderHelper_PawnTile(
            ReadyTileColorName, ReadyTilePulseName, (pawn, _) => AdvanceTurnToTeam(pawn.Team)
        );
    }

    [Test]
    public void TestShader_SelectedValidTile()
    {
        TestShaderHelper_PawnTile(
            SelectedTileColorName, SelectedTilePulseName, (pawn, _) =>
            {
                AdvanceTurnToTeam(pawn.Team);
                ClickOnTile(pawn.Tile);
            }
        );
    }

    [Test]
    public void TestShader_SelectedInvalidTile()
    {
        SetShaderParameter(SelectedTileInvalidScalarName, 1f);
        TestShaderHelper_PawnTile(
            SelectedTileColorName, SelectedTilePulseName, (thisPawn, thatPawn) =>
            {
                AdvanceTurnToTeam(thatPawn.Team);
                ClickOnTile(thisPawn.Tile);
            }
        );
    }

    [Test]
    public void TestShader_MovementRange()
    {
        TestShaderHelper_ActionRange(ActionType.Move, false);
    }

    [Test]
    public void TestShader_MovementValid()
    {
        TestShaderHelper_ActionRange(ActionType.Move, true);
    }

    [Test]
    public void TestShader_AttackRange()
    {
        TestShaderHelper_ActionRange(ActionType.Attack, false);
    }

    [Test]
    public void TestShader_AttackValid()
    {
        TestShaderHelper_ActionRange(ActionType.Attack, true);
    }

    [Test]
    public void TestShader_Target_MoveValid()
    {
        TestShaderHelper_Target(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(1, 1)
        );
    }

    [Test]
    public void TestShader_Target_MoveInvalid()
    {
        TestShaderHelper_Target(ActionType.Move, _pawnPlayer, _pawnCpu.Tile);
    }

    [Test]
    public void TestShader_Target_AttackValid()
    {
        TestShaderHelper_Target(ActionType.Move, _pawnCpu, _pawnPlayer.Tile);
    }

    [Test]
    public void TestShader_Target_AttackInvalid()
    {
        TestShaderHelper_Target(ActionType.Move, _pawnCpu, _pawnCpu.Tile + new Vector2I(1, 1));
    }

    [Test]
    public void TestShader_TilePath_Move0()
    {
        TestShaderHelper_TilePath(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(-3, 1)
        );
    }

    [Test]
    public void TestShader_TilePath_Move1()
    {
        TestShaderHelper_TilePath(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(2, -3)
        );
    }

    [Test]
    public void TestShader_TilePath_Move2()
    {
        TestShaderHelper_TilePath(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(2, 2)
        );
    }

    [Test]
    public void TestShader_TilePath_Move3()
    {
        TestShaderHelper_TilePath(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(-4, 0)
        );
    }

    [Test]
    public void TestShader_TilePath_Move4()
    {
        TestShaderHelper_TilePath(
            ActionType.Move, _pawnPlayer, _pawnPlayer.Tile + new Vector2I(0, -3)
        );
    }

    [Test]
    public void TestShader_TilePath_Attack()
    {
        TestShaderHelper_TilePath(ActionType.Attack, _pawnCpu, _pawnPlayer.Tile);
    }

    [Test]
    public void TestShader_Aoe_Ambush()
    {
        TestShaderHelper_Aoe(ActionType.Ambush, _pawnPlayer, _pawnPlayer.Tile);
    }

    private void TestShaderHelper_PawnTile(
        StringName colorName, StringName pulseName, Action<IPawn, IPawn> beforeTurn)
    {
        SetShaderParameter(CheckerboardScalarName, 0f);
        SetShaderParameter(colorName, Colors.Red);
        SetShaderParameter(pulseName, 0f);
        beforeTurn.Invoke(_pawnPlayer, _pawnCpu);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    Assert.AreEqual(GetColorAtTile(_pawnPlayer.Tile), Colors.Red);
                    Assert.AreNotEqual(GetColorAtTile(_pawnCpu.Tile), Colors.Red);
                    beforeTurn.Invoke(_pawnCpu, _pawnPlayer);
                    this.GetTestRunner()
                        .WaitFrames(
                            2, () =>
                            {
                                Assert.AreEqual(GetColorAtTile(_pawnCpu.Tile), Colors.Red);
                                Assert.AreNotEqual(GetColorAtTile(_pawnPlayer.Tile), Colors.Red);
                            }
                        );
                }
            );
    }

    private void TestShaderHelper_ActionRange(ActionType actionType, bool isValidityTest)
    {
        SetShaderParameter(CheckerboardScalarName, 0f);
        SetShaderParameter(ActionRangeTileColorName, Colors.Green);
        SetShaderParameter(ActionRangeTilePulseName, 0f);
        SetShaderParameter(ActionRangeInvalidScalarName, isValidityTest ? 0f : 1f);
        TestShaderHelper_ActionRange_Turn(
            actionType, isValidityTest, _pawnPlayer,
            () => TestShaderHelper_ActionRange_Turn(actionType, isValidityTest, _pawnCpu)
        );
    }

    private void TestShaderHelper_ActionRange_Turn(
        ActionType actionType, bool isValidityTest, IPawn pawn, Action? next = null)
    {
        AdvanceTurnToTeam(pawn.Team);
        ClickOnTile(pawn.Tile);
        OnActionButtonPressed(actionType);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    foreach (Vector2I tile in GetAllTiles())
                    {
                        if (isValidityTest
                            ? pawn.CanPerformActionAtTile(actionType, tile)
                            : pawn.IsTileInActionRange(actionType, tile))
                        {
                            Assert.AreEqual(
                                GetColorAtTile(tile), Colors.Green, $"{pawn.Team} - {tile}"
                            );
                        }
                        else
                        {
                            Assert.AreNotEqual(
                                GetColorAtTile(tile), Colors.Green, $"{pawn.Team} - {tile}"
                            );
                        }
                    }
                    next?.Invoke();
                }
            );
    }

    private void TestShaderHelper_Target(ActionType actionType, IPawn pawn, Vector2I tile)
    {
        SetShaderParameter(TargetTileColorName, Colors.Blue);
        SetShaderParameter(TargetTilePulseName, 0f);
        TestShaderHelper_SelectAndTargetTile(actionType, pawn, tile);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    if (SelectionState.IsTileTargeted(tile.X, tile.Y))
                    {
                        Assert.AreEqual(GetColorAtTile(tile), Colors.Blue, $"{tile}");
                    }
                    else
                    {
                        Assert.AreNotEqual(GetColorAtTile(tile), Colors.Blue, $"{tile}");
                    }
                }
            );
    }

    private void TestShaderHelper_TilePath(ActionType actionType, IPawn pawn, Vector2I tile)
    {
        SetShaderParameter(TargetTileColorName, Colors.Cyan);
        SetShaderParameter(TargetTilePulseName, 0f);
        SetShaderParameter(TilePathScalarName, 1f);
        TestShaderHelper_SelectAndTargetTile(actionType, pawn, tile);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    foreach (Vector2I connectingTile in GetConnectingTiles(pawn.Tile, tile, false))
                    {
                        Assert.AreEqual(
                            GetColorAtTile(connectingTile), Colors.Cyan, $"{connectingTile}"
                        );
                    }
                }
            );
    }

    private void TestShaderHelper_Aoe(ActionType actionType, IPawn pawn, Vector2I tile)
    {
        SetShaderParameter(AoeTileColorName, Colors.Magenta);
        SetShaderParameter(AoeTilePulseName, 0f);
        TestShaderHelper_SelectAndTargetTile(actionType, pawn, tile);
        this.GetTestRunner()
            .WaitFrames(
                2, () =>
                {
                    foreach (Vector2I aoeTile in GetAllTiles())
                    {
                        if (SelectionState.IsTileInEffectArea(aoeTile.X, aoeTile.Y))
                        {
                            Assert.AreEqual(GetColorAtTile(aoeTile), Colors.Magenta, $"{aoeTile}");
                        }
                        else
                        {
                            Assert.AreNotEqual(
                                GetColorAtTile(aoeTile), Colors.Magenta, $"{aoeTile}"
                            );
                        }
                    }
                }
            );
    }

    private void TestShaderHelper_SelectAndTargetTile(
        ActionType actionType, IPawn pawn, Vector2I tile)
    {
        SetShaderParameter(CheckerboardScalarName, 0f);
        AdvanceTurnToTeam(pawn.Team);
        ClickOnTile(pawn.Tile);
        OnActionButtonPressed(actionType);
        ClickOnTile(tile);
    }

    private void SetShaderParameter<[MustBeVariant] T>(StringName name, T value)
    {
        Variant prev = ShaderMaterial.GetShaderParameter(name);
        _afterEachActions.Add(() => ShaderMaterial.SetShaderParameter(name, prev));
        ShaderMaterial.SetShaderParameter(name, Variant.From(value));
    }

    private Color GetColorAtTile(Vector2I tile)
    {
        Vector2 pos = GlobalPositionToViewportPosition(TileCoordinatesToLocalPosition(tile));
        if (pos.X < 0f
            || pos.Y < 0f
            || (int)pos.X >= GetViewport().GetTexture().GetWidth()
            || (int)pos.Y >= GetViewport().GetTexture().GetHeight())
        {
            return Colors.Black;
        }
        return GetViewport().GetTexture().GetImage().GetPixel((int)pos.X, (int)pos.Y);
    }
}
