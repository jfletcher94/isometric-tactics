using Godot;

using System.Diagnostics.CodeAnalysis;

namespace IsometricTactics;

public static class CmdLineHelper
{
    public static bool TryGetRawValue(string key, [MaybeNullWhen(false)] out string value)
    {
        string formattedKey = $"--{key}=";
        foreach (string arg in OS.GetCmdlineUserArgs())
        {
            if (!arg.StartsWith(formattedKey))
            {
                continue;
            }
            
            value = arg[formattedKey.Length..];
            return true;
        }
        value = null;
        return false;
    }
}
