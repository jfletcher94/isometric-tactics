using Godot;

namespace IsometricTactics;

public class AttackAction : IPawnAction
{
    private readonly Vector2 _direction;
    private float _timeRemaining;

    public AttackAction(Vector2 ownerToTarget, float timeRemaining)
    {
        _direction = ownerToTarget;
        _timeRemaining = timeRemaining;
    }

    public ActionType GetActionType()
    {
        return ActionType.Attack;
    }

    public void Update(IPawn pawn, double delta)
    {
        _timeRemaining -= (float)delta;
    }

    public bool IsOngoing()
    {
        return _timeRemaining > 0f;
    }

    public bool OnFinished(IPawn pawn)
    {
        pawn.GetOwnedEngagement()?.StartEngagement();
        return true;
    }

    public Vector2 GetDirection()
    {
        return _direction;
    }
}
