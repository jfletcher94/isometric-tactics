using System.Collections.Generic;
using System.Linq;

using Godot;

namespace IsometricTactics;

public partial class ButtonPanel : Panel
{
    [Export] public TileMapHelper LevelTileMap { get; private set; }

    private List<Button> _buttons;

    public override void _Ready()
    {
        _buttons = FindChildren("*", recursive: false).OfType<Button>().ToList();
    }

    public void OnUpdateSelected()
    {
        foreach (Button button in _buttons)
        {
            if (button is ActionMenuButton actionMenuButton)
            {
                button.Disabled =
                    !LevelTileMap.CanSelectedTilePerformAction(actionMenuButton.ActionType);
            }
            else if (button is CommitPanelButton commitPanelButton)
            {
                button.Disabled = !LevelTileMap.IsCommitPanelButtonValid(commitPanelButton.Type);
            }
            else
            {
                GD.PrintErr($"Unhandled button type: {button.GetType()}");
            }
        }
    }
}
