using Godot;

namespace IsometricTactics;

public class MoveAction : IPawnAction
{
    private readonly Vector2 _direction;
    private float _timeRemaining;
    private Vector2I? _interruptTile;

    public MoveAction(IPawn pawn, Vector2I fromTile, Vector2I toTile, float moveSpeed)
    {
        Vector2 oldToNew = (Vector2)pawn.LevelTileMap().GetFromTileToTile(fromTile, toTile)!;
        _timeRemaining = oldToNew.Length() / moveSpeed;
        _direction = oldToNew / _timeRemaining;
        _interruptTile = FindInterruptTile(pawn, fromTile, toTile);
        if (_interruptTile == null)
        {
            return;
        }

        pawn.Interrupt();
        _timeRemaining *=
            ((Vector2)pawn.LevelTileMap().GetFromTileToTile(fromTile, _interruptTile)!).Length()
            / oldToNew.Length();
    }

    // Bresenham's line algorithm
    private Vector2I? FindInterruptTile(IPawn pawn, Vector2I fromTile, Vector2I toTile)
    {
        foreach (Vector2I tile in pawn.LevelTileMap().GetConnectingTiles(fromTile, toTile, true))
        {
            if ((tile == toTile || pawn.CanPerformActionAtTile(ActionType.Move, tile))
                && pawn.GetAffectingEffectAreaAtTile(tile, out IPawn? ignored))
            {
                return tile;
            }
        }

        return null;
    }

    public ActionType GetActionType()
    {
        return ActionType.Move;
    }

    public void Update(IPawn pawn, double delta)
    {
        _timeRemaining -= (float)delta;
        pawn.MoveByIncrement(_direction * (float)delta);
        // TODO check for interrupts
    }

    public bool IsOngoing()
    {
        return _timeRemaining > 0f;
    }

    public bool OnFinished(IPawn pawn)
    {
        if (_interruptTile != null)
        {
            pawn.TryMoveToTile((Vector2I)_interruptTile);
        }
        pawn.UpdatePositionFromTile();
        return _interruptTile == null;
    }

    public Vector2 GetDirection()
    {
        return _direction;
    }
}
