using System;

using Godot;

namespace IsometricTactics;

[GlobalClass]
public partial class CardinalDirectionsRangFunc : RangeFunc
{
    [Export] private bool _cardinals = true;
    [Export] private bool _diagonals = false;

    public CardinalDirectionsRangFunc() {}

    public CardinalDirectionsRangFunc(bool cardinals, bool diagonals)
    {
        _cardinals = cardinals;
        _diagonals = diagonals;
    }

    public override bool IsTileInRange(Vector2I fromTile, Vector2I toTile)
    {
        return (_cardinals && (fromTile.X == toTile.X || fromTile.Y == toTile.Y))
            || (_diagonals && Math.Abs(toTile.X - fromTile.X) == Math.Abs(toTile.Y - fromTile.Y));
    }
}
