using System.Collections.Generic;
using System.Linq;

using Godot;

namespace IsometricTactics;

public interface IPawn
{
    public Error Connect(StringName signal, Callable callable, uint flags = 0); // GodotObject
    public Node2D AsNode();

    public TeamType Team { get; }
    public Vector2I Tile { get; }
    public PawnSprite Sprite { get; }
    public int Focus { get; }
    public int AvailableActions { get; }

    public bool TryMoveToTile(Vector2I tile);
    public TileMapHelper LevelTileMap();

    public TeamParent TeamParent();

    public bool IsReady()
    {
        return LevelTileMap().ActiveTeam == Team && AvailableActions > 0;
    }

    public float GetAttackPower(bool isAttackOwner);
    public float GetDefensePower(bool isAttackOwner);
    public Engagement? GetOwnedEngagement();

    public IEnumerable<Engagement> GetEngagementsAsTarget()
    {
        return LevelTileMap()
            .GetAllPawns()
            .Select(pawn => pawn.GetOwnedEngagement())
            .Where(
                engagement => engagement != null
                    && engagement.Owner != this
                    && engagement.Target == this
            )!;
    }

    public bool IsEngaged();

    public bool IsEngagedAsTarget();

    public bool IsEngagedAsOwner();

    public void NotifyEngagementStarted(Engagement engagement);

    public void NotifyEngagementEnded(Engagement engagement);

    public void NotifyEngagementIteration(Engagement engagement);

    public void AdvanceTurn();

    public bool IsFriendly(IPawn? pawn)
    {
        return pawn != null && Team == pawn.Team && Team != TeamType.None;
    }

    public bool IsEnemy(IPawn? pawn)
    {
        return pawn != null
            && Team != pawn.Team
            && Team != TeamType.None
            && pawn.Team != TeamType.None;
    }

    public bool IsNeutral(IPawn? pawn)
    {
        return pawn == null || Team == TeamType.None || pawn.Team == TeamType.None;
    }

    public StringName ActivateReaction(IPawn pawn);

    public bool IsTileInEffectArea(ActionType actionType, Vector2I? target, int x, int y)
    {
        return IsTileInEffectArea(actionType, target, new Vector2I(x, y));
    }

    public bool IsTileInEffectArea(ActionType actionType, Vector2I? target, Vector2I tile);

    public bool CanCounterActionFromTile(ActionType actionType, Vector2I tile);

    public bool CanPerformAction(ActionType actionType);

    public bool IsTileInActionRange(ActionType actionType, int x, int y)
    {
        return IsTileInActionRange(actionType, new Vector2I(x, y));
    }

    public bool IsTileInActionRange(ActionType actionType, Vector2I tile);

    public bool CanPerformActionAtTile(ActionType actionType, int x, int y)
    {
        return CanPerformActionAtTile(actionType, new Vector2I(x, y));
    }

    public bool CanPerformActionAtTile(ActionType actionType, Vector2I tile);

    public StringName PerformActionAtTile(ActionType actionType, Vector2I tile, int actionCost = 1);

    public bool GetAffectingEffectAreaAtTile(Vector2I tile, out IPawn? result);

    public void MoveByIncrement(Vector2 delta);

    public void UpdatePositionFromTile();

    public void SetSelected(bool selected)
    {
        Sprite.SetSelected(selected);
    }

    public void Interrupt();

    public void KnockOut();
}
