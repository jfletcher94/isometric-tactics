using Godot;

namespace IsometricTactics;

public partial class ActionMenuButton : Button
{
    [Export] public ActionType ActionType { get; private set; } = ActionType.None;

    public void OnButtonPressed()
    {
        GetParent<ButtonPanel>().LevelTileMap.OnActionButtonPressed(ActionType);
    }
}
