using Godot;

namespace IsometricTactics;

public class DisengageAction : IPawnAction
{
    private float _timeRemaining;

    public DisengageAction(float timeRemaining)
    {
        _timeRemaining = timeRemaining;
    }

    public ActionType GetActionType()
    {
        return ActionType.Disengage;
    }

    public void Update(IPawn pawn, double delta)
    {
        _timeRemaining -= (float)delta;
    }

    public bool IsOngoing()
    {
        return _timeRemaining > 0f;
    }

    public bool OnFinished(IPawn pawn)
    {
        pawn.GetOwnedEngagement()?.EndEngagement();
        return true;
    }

    public Vector2 GetDirection()
    {
        return Vector2.Zero;
    }
}
