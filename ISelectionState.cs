using System.Collections.Generic;

using Godot;

namespace IsometricTactics;

// TODO assembly
public interface ISelectionState
{
    internal static readonly StringName ActionPerformedSignalName =
        new("Selection State Action Performed");

    // One per player
    public static ISelectionState InitNew()
    {
        return new SelectionStateNone();
    }

    public Type StateType();

    public ISelectionState Select(TileMapHelper tileMap, Vector2I? tile);

    public ISelectionState Deselect();
    public ISelectionState Reset();

    public ISelectionState ChooseAction(ActionType actionType);

    public Vector2I? GetSelectedTile();

    public bool CanEndTurn()
    {
        return true;
    }

    public bool CanValidActionBeCommitted(TileMapHelper tileMap)
    {
        return false;
    }

    public (Node, StringName)? PerformAction(TileMapHelper tileMap)
    {
        return null;
    }

    public IEnumerable<Vector2I> GetPathToTarget(TileMapHelper tileMap, bool inclusive)
    {
        yield break;
    }

    public bool IsTileSelected(int x, int y)
    {
        return GetSelectedTile()?.X == x && GetSelectedTile()?.Y == y;
    }

    public bool IsTileInActionRange(int x, int y)
    {
        return false;
    }

    public bool CanPerformActionAtTile(int x, int y)
    {
        return false;
    }

    public bool IsTileTargeted(int x, int y)
    {
        return false;
    }

    bool IsTileInEffectArea(int x, int y)
    {
        return false;
    }

    public bool IsCursorTileValid(Vector2I? tile)
    {
        return true;
    }

    public bool IsSelectedTileValid()
    {
        return false;
    }

    public enum Type
    {
        // TODO naming?
        Void = -1,
        NoneType,
        NeutralType,
        FriendlyType,
        Count
    }
}
