using Godot;

namespace IsometricTactics;

[GlobalClass]
public abstract partial class RangeFunc : Resource
{
    public abstract bool IsTileInRange(Vector2I fromTile, Vector2I toTile);
}
