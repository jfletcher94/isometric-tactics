FROM mcr.microsoft.com/dotnet/sdk:8.0

ARG GODOT_VERSION

WORKDIR /godot

RUN apt-get -qq update && apt-get -qq install -y \
    wget \
    unzip \
    clang \
    xvfb \
    llvm \
    zlib1g-dev \
    x11-utils \
    x11-xkb-utils \
    mesa-utils \
    libgl1-mesa-dri \
    libgl1-mesa-glx \
    libglu1-mesa \
    libxrandr2 \
    libxrender1 \
    libxi6 \
    libxtst6 \
    libxss1 \
    libxcursor1 \
    libgconf-2-4 \
    libnss3 \
    libasound2
RUN wget -nv -O godot-mono.zip https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_mono_linux_x86_64.zip && \
    unzip -q godot-mono.zip && \
    rm godot-mono.zip
RUN wget -nv -O export-templates.tpz https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_mono_export_templates.tpz && \
    unzip -q export-templates.tpz && \
    rm export-templates.tpz
RUN ln -s /godot/Godot_v${GODOT_VERSION}-stable_mono_linux_x86_64/Godot_v${GODOT_VERSION}-stable_mono_linux.x86_64 /usr/local/bin/godot

ENV GALLIUM_DRIVER="llvmpipe" \
    DISPLAY=":99" \
    LIBGL_ALWAYS_SOFTWARE=1
